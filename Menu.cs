﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;




namespace VirtualAutoCenter


{
    class Menu
    {
        VechiclesList Lista = new VechiclesList();
        


        public Menu()
        {
            this.menuMainSection();

        }

        private void menuMainSection()
        {

            Console.WriteLine("=========================================");
            Console.WriteLine("                   MENU                  ");
            Console.WriteLine("=========================================");
            Console.WriteLine("                                         ");
            Console.WriteLine("1.Wyświetl listę pojazdów                ");
            Console.WriteLine("2.Dodaj nowy pojazd                      ");
            Console.WriteLine("3.Wyszukaj pojazdy                       ");
            Console.WriteLine("4.Sprzedaj pojazd                        ");
            Console.WriteLine("                                         ");
            Console.WriteLine("=========================================");
            Console.WriteLine("0.Opuść Menu");
            Console.WriteLine("=========================================");


            int choice = 0;

            bool check = false;

            do
            {
                try
                {

                    choice = int.Parse(Console.ReadLine());
                    check = true;
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych");
                    Console.Clear();
                    this.menuMainSection();

                }
            } while (check == false);

            switch (choice)
            {


                case 1:
                    List<Pojazd> List = Lista.getVechicleList();
                    Console.Clear();
                    Console.WriteLine("=============================================");
                    Console.WriteLine("               WSZYSKIE POJAZDY              ");
                    Console.WriteLine("=============================================");
                    Console.WriteLine("                                             ");
                    
                    for(int i = 0; i<List.Count(); i++)
                    {
                        Console.WriteLine(List[i].wyswietlMarke() + " " + List[i].wyswietlModel() + " " + List[i].RokProdukcji);
                    }
                    Console.WriteLine("                                             ");
                    Console.WriteLine("                                             ");
                    Console.WriteLine("=============================================");
                    Console.WriteLine("0.Wstecz                                     ");
                    Console.WriteLine("=============================================");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                    Console.Clear();
                    this.menuMainSection();
                    break;

                case 2:
                    Console.Clear();
                    this.addNewVechicle();
                    break;

                case 3:
                    Console.Clear();
                    this.searchVechicle();
                    break;

                case 4:
                    Console.Clear();
                    this.sellVechicle();
                    break;

                default:
                    
                    Console.Clear();
                    this.menuMainSection();


                    break;
            }

        }

        private void showAllVechicels()
        {
           

        }

        private void searchVechicle()
        {
            Console.WriteLine("=============================================");
            Console.WriteLine("                     MENU                    ");
            Console.WriteLine("=============================================");
            Console.WriteLine("                                             ");
            Console.WriteLine("1. Wyszukaj po przedziale cenowym: od do     ");
            Console.WriteLine("1. Wyszukaj po przedziale rocznikowym: od do ");
            Console.WriteLine("                                             ");
            Console.WriteLine("                                             ");
            Console.WriteLine("=============================================");
            Console.WriteLine("0.Wstecz                                     ");
            Console.WriteLine("=============================================");

            int choice = 0;
            bool check = false;
            do
            {
                try
                {
                    Console.WriteLine("Wybierz: ");
                    choice = int.Parse(Console.ReadLine());
                    check = true;
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych");

                }
            } while (check == false);

            switch (choice)
            {
                case 0:
                    Console.Clear();
                    this.menuMainSection();

                    break;
                case 1:
                    Console.Clear();
                    Console.WriteLine("Wyszukiwanie 1");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                    Console.Clear();
                    this.menuMainSection();

                    break;
                case 2:
                    Console.Clear();
                    Console.WriteLine("Wyszukiwanie 2");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                    Console.Clear();
                    this.menuMainSection();
                    break;
                default:
                    Console.WriteLine("Brak opcji");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                    Console.Clear();
                    this.menuMainSection();
                    break;
            }

            }

        private void sellVechicle()
        {
            Console.WriteLine("=========================================");
            Console.WriteLine("                   MENU                  ");
            Console.WriteLine("=========================================");
            Console.WriteLine("=============SPRZEDAJ POJAZD===========");
            this.showListVechiclesToSell();
            Console.WriteLine("                                         ");
            Console.WriteLine("                                         ");
            Console.WriteLine("                                         ");
            Console.WriteLine("                                         ");
            Console.WriteLine("                                         ");
            Console.WriteLine("=========================================");
            Console.WriteLine("0.Wstecz                                 ");
            Console.WriteLine("=========================================");

            int choice = 0;
            bool check = false;
            do
            {
                try
                {
                    Console.WriteLine("Wybierz: ");
                    choice = int.Parse(Console.ReadLine());
                    check = true;
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych");

                }
            } while (check == false);

            switch (choice)
            {
                case 0:
                    Console.Clear();
                    this.menuMainSection();
                    break;

                default:
                    Console.WriteLine("Brak opcji w menu");
                    Console.Clear();
                    this.addNewVechicle();
                    break;


            }

        }

        private void addNewVechicle()
        {

            Console.WriteLine("=========================================");
            Console.WriteLine("                   MENU                  ");
            Console.WriteLine("=========================================");
            Console.WriteLine("=============DODAJ NOWY POJAZD===========");
            this.showListAvaliableVechicles();
            Console.WriteLine("                                         ");
            Console.WriteLine("                                         ");
            Console.WriteLine("                                         ");
            Console.WriteLine("                                         ");
            Console.WriteLine("                                         ");
            Console.WriteLine("=========================================");
            Console.WriteLine("0.Wstecz                                 ");
            Console.WriteLine("=========================================");

            int choice = 0;
            bool check = false;
            do
            {
                try
                {
                    Console.WriteLine("Wybierz: ");
                    choice = int.Parse(Console.ReadLine());
                    check = true;
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych");

                }
            } while (check == false);

            switch (choice)
            {
                case 0:
                    Console.Clear();
                    this.menuMainSection();
                    break;
                    
                default:
                    Console.WriteLine("Brak opcji w menu");
                    Console.Clear();
                    this.addNewVechicle();
                    break;


            }

        }

        private void showListAvaliableVechicles()
        {

        }

        private void showListVechiclesToSell()
        {

        }
    }
}
