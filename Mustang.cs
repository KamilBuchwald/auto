﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualAutoCenter
{
    class Mustang : Ford
    {
        public Mustang(int iRokProdukcji) : base(iRokProdukcji)
        {
            inputRokProdukcji = iRokProdukcji;
        }

        public Mustang(int iRokProdukcji, float fMarza) : base(iRokProdukcji)
        {
            inputRokProdukcji = iRokProdukcji;
            inputMarza = fMarza;
        }
        public Mustang(int iRokProdukcji, float fMarza, float fCenaZakupu) : base(iRokProdukcji)
        {
            inputRokProdukcji = iRokProdukcji;
            inputMarza = fMarza;
            inputCenaZakupu = fCenaZakupu;
        }

        public override string wyswietlModel()
        {
            String st = "Mustang";
            return st;
        }
    }
}
