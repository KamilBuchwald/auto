﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualAutoCenter
{
    abstract class Ford : Samochod
    {
        public Ford(int iRokProdukcji) : base(iRokProdukcji)
        {
        }

        public Ford(int iRokProdukcji, float fMarza) : base(iRokProdukcji)
        {

        }
        public Ford(int iRokProdukcji, float fMarza, float fCenaZakupu) : base(iRokProdukcji)
        {

        }

        public override string wyswietlMarke()
        {
            String s = "Ford";
            return s;
        }
    }
}
